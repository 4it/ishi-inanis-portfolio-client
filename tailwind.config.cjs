module.exports = {
  content: [
    './index.html',
    './src/**/*.{html,js,ts,svelte}'
  ],
  theme: {
    extend: {
      dropShadow: {
        'hard': '3px 3px 1px rgb(0 0 0 / .3)'
      }
    }
  },
  plugins: []
}
